﻿using Lab4.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab4
{
    public partial class Form1 : Form
    {
       StudentsContexDB context = new StudentsContexDB();
        public Form1()
        {
            InitializeComponent();
        }

        private void FillFacultyCombobox(List<Faculty> listFalcultys)
        {
            this.cboKhoa.DataSource = listFalcultys;
            this.cboKhoa.DisplayMember = "FacultyName";
            this.cboKhoa.ValueMember = "FacultyID";
        }

        private void BindGrid(List<Student> listStudent)
        {
            dataGridView1.Rows.Clear();
            foreach (var item in listStudent)
            {
                int index = dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells[0].Value = item.StudentID;
                dataGridView1.Rows[index].Cells[1].Value = item.FullName;
                dataGridView1.Rows[index].Cells[2].Value = item.Faculty.FacultyName;
                dataGridView1.Rows[index].Cells[3].Value = item.AverageScore;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                    StudentsContexDB context = new StudentsContexDB();
                List<Faculty> listFacutys = context.Faculty.ToList();
                List<Student> listStudent = context.Student.ToList();
                FillFacultyCombobox(listFacutys);
                BindGrid(listStudent);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            try
            {
                Student s = new Student()
                {
                    StudentID = txtMSSV.Text.Trim(),
                    FullName = txtTSV.Text.Trim(),
                    AverageScore = double.Parse(txtDTB.Text.Trim()),
                    FacultyID = int.Parse(cboKhoa.SelectedValue.ToString())
                };
                context.Student.Add(s);
                context.SaveChanges();

                List<Student> listStudents = context.Student.Include("Faculty").ToList();
                BindGrid(listStudents);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            try
            {
                Student student = context.Student.FirstOrDefault(s => s.StudentID == txtMSSV.Text.Trim());
                if (student != null)
                {
                    student.FullName = txtTSV.Text.Trim();
                    student.AverageScore = double.Parse(txtDTB.Text.Trim());
                    student.FacultyID = int.Parse(cboKhoa.SelectedValue.ToString());
                    context.SaveChanges();

                    List<Student> listStudents = context.Student.Include("Faculty").ToList();
                    BindGrid(listStudents);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            var ask = MessageBox.Show("Ban co muon thoat ?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (ask == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            {
                StudentsContexDB context = new StudentsContexDB();
                Student dbDelete = context.Student.FirstOrDefault(p => p.StudentID.ToString() == txtMSSV.Text.ToString());
                if (dbDelete != null)
                {
                    context.Student.Remove(dbDelete);
                    context.SaveChanges();
                    List<Student> listStudents = context.Student.ToList();
                    BindGrid(listStudents);
                }
            }
        }
     
        
        private void thoatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ask = MessageBox.Show("Ban co muon thoat ?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (ask == DialogResult.Yes)
            {
                this.Close();
            }
        }
    }
}
